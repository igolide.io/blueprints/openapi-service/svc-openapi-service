package io.igolide.blueprints.openapi.core.security

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtAuthenticationFilter : OncePerRequestFilter() {
    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        val token = tokenFromHeader(request)
        if (token != null) {
            val auth = JwtAuthentication(token)
            SecurityContextHolder.getContext().authentication = auth
        }
        filterChain.doFilter(request, response)
    }

    private fun tokenFromHeader(request: HttpServletRequest): String? {
        val token = request.getHeader("authorization") ?: return null
        val spitedToken = token.split(' ')
        return when (spitedToken.count()) {
            1 -> spitedToken[0].trim()
            2 -> spitedToken[1].trim()
            else -> null
        }
    }
}

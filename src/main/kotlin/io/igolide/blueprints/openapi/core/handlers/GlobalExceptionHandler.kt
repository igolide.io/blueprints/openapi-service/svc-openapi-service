package io.igolide.blueprints.openapi.core.handlers

import io.igolide.blueprints.openapi.service.server.model.InternalServerError
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import java.util.UUID
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@ControllerAdvice
@Order(Ordered.LOWEST_PRECEDENCE)
class GlobalExceptionHandler {
    @ExceptionHandler(Throwable::class)
    fun handle(
        ex: Throwable,
        request: HttpServletRequest?,
        response: HttpServletResponse?
    ): ResponseEntity<InternalServerError> {
        return ResponseEntity(
            InternalServerError().also {
                it.message = ex.message
                it.requestId = UUID.randomUUID().toString()
            },
            HttpStatus.INTERNAL_SERVER_ERROR
        )
    }
}

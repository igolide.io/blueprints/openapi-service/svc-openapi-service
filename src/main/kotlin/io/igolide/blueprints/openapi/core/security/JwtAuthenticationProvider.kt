package io.igolide.blueprints.openapi.core.security

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import java.security.KeyFactory
import java.security.interfaces.RSAPublicKey
import java.security.spec.X509EncodedKeySpec
import java.util.Base64

class JwtAuthenticationProvider : AuthenticationProvider {
    private val publicKey: String = System.getProperty("RSA_PUBLIC_KEY") ?: System.getenv("RSA_PUBLIC_KEY")
    private val algorithm: Algorithm
        get() {
            val kFactory: KeyFactory = KeyFactory.getInstance("RSA")
            val yourKey: ByteArray = Base64.getDecoder().decode(publicKey)
            val spec = X509EncodedKeySpec(yourKey)
            val publicKey = kFactory.generatePublic(spec) as RSAPublicKey
            return Algorithm.RSA256(publicKey, null)
        }
    private var verifier: JWTVerifier = JWT.require(algorithm).build()

    override fun authenticate(authentication: Authentication): Authentication {
        val auth = authentication as JwtAuthentication
        verify(authentication)
        auth.isAuthenticated = true
        return auth
    }

    override fun supports(authentication: Class<*>?): Boolean {
        return JwtAuthentication::class.java.isAssignableFrom(authentication!!)
    }

    private fun verify(auth: JwtAuthentication) {
        try {
            verifier.verify(auth.credentials as String)
        } catch (e: Exception) {
            throw BadCredentialsException("Access denied")
        }
    }
}

package io.igolide.blueprints.openapi.core.security

import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.core.AuthenticationException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
class AccessExceptionHandler {
    @ExceptionHandler(AccessDeniedException::class)
    fun handleAccessDeniedException(
        ex: AccessDeniedException,
        request: HttpServletRequest?,
        response: HttpServletResponse?
    ): ResponseEntity<String> {
        return ResponseEntity(
            ex.message,
            HttpStatus.FORBIDDEN
        )
    }

    @ExceptionHandler(AuthenticationException::class)
    fun handleAuthenticationException(
        ex: AuthenticationException,
        request: HttpServletRequest?,
        response: HttpServletResponse?
    ): ResponseEntity<String> {
        return ResponseEntity(
            ex.message,
            HttpStatus.UNAUTHORIZED
        )
    }
}

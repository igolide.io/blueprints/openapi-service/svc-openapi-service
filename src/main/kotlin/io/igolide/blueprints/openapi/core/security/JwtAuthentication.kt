package io.igolide.blueprints.openapi.core.security

import com.auth0.jwt.JWT
import com.auth0.jwt.interfaces.DecodedJWT
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.oauth2.server.resource.BearerTokenAuthenticationToken

class JwtAuthentication(token: String?) : BearerTokenAuthenticationToken(token) {
    private var details: DecodedJWT? = null
    private var subject: String? = null
    private var authorities: MutableList<SimpleGrantedAuthority>? = null

    init {
        try {
            val decodedJWT = JWT.decode(token)
            details = decodedJWT
            subject = decodedJWT.subject
            authorities = decodedJWT
                .getClaim("roles")
                ?.asArray(String::class.java)
                ?.map { SimpleGrantedAuthority("ROLE_${it.uppercase()}") }
                ?.toMutableList()
        } catch (exp: Exception) {
            this.subject = "unknown"
        }
    }

    override fun getAuthorities(): MutableList<SimpleGrantedAuthority>? {
        return authorities
    }
}

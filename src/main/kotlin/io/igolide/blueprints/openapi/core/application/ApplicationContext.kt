package io.igolide.blueprints.openapi.core.application

import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.stereotype.Component

@Component
class ApplicationContext : ApplicationContextAware {
    override fun setApplicationContext(applicationContext: ApplicationContext) {
        CONTEXT = applicationContext
    }

    companion object {
        private var CONTEXT: ApplicationContext? = null

        private fun getContext(): ApplicationContext {
            return CONTEXT!!
        }

        fun <T> getBean(beanClass: Class<T>): T {
            return getContext().getBean(beanClass)
        }
    }
}

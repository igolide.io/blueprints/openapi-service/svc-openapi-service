package io.igolide.blueprints.openapi.configuration

import io.igolide.blueprints.openapi.service.server.service.AccountApiController
import org.openapitools.configuration.HomeController
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackageClasses = [AccountApiController::class, HomeController::class])
class ApiScanConfiguration

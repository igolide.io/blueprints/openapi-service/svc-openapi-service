package io.igolide.blueprints.openapi.configuration

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Scope

@Configuration
class LoggerConfig {
    @Bean
    @Scope("prototype")
    fun logger(): Logger {
        return LoggerFactory.getLogger("DEFAULT")
    }
}

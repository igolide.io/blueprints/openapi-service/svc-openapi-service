package io.igolide.blueprints.openapi.services.account

import io.igolide.blueprints.openapi.core.application.ApplicationContext
import io.igolide.blueprints.openapi.service.server.model.Account
import io.igolide.blueprints.openapi.service.server.service.AccountApiDelegate
import io.igolide.blueprints.openapi.services.account.lib.operations.GetAccountsOperation
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Service

@Service
class AccountService : AccountApiDelegate {
    @Secured("ROLE_USER")
    override fun getAccounts(offset: Int, limit: Int, byId: MutableList<Int>?): ResponseEntity<MutableList<Account>> {
        return ApplicationContext.getBean(GetAccountsOperation::class.java).execute(offset, limit, byId)
    }
}

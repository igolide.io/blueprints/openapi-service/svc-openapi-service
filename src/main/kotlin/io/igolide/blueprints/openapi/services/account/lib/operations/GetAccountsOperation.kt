package io.igolide.blueprints.openapi.services.account.lib.operations

import io.igolide.blueprints.openapi.service.server.model.Account
import io.igolide.blueprints.openapi.service.server.model.AccountData
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.context.annotation.Scope
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.sql.Timestamp

@Service
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
class GetAccountsOperation {
    fun execute(offset: Int, limit: Int, byId: MutableList<Int>?): ResponseEntity<MutableList<Account>> {
        val accounts = mutableListOf<Account>()
        for (i in 1..20) {
            accounts.add(makeAccount(i))
        }
        return ResponseEntity(accounts, HttpStatus.OK)
    }

    fun makeAccount(id: Int): Account {
        val account = Account()
        account.id = id
        account.email = "john.galt@igolide.io"
        account.registrationDate = Timestamp(System.currentTimeMillis())
        account.data = AccountData().apply {
            this.name = "John Galt"
        }
        return account
    }
}

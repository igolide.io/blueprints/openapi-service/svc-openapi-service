package io.igolide.blueprints.openapi.core

import io.igolide.blueprints.openapi.service.client.invoker.ApiClient
import io.igolide.blueprints.openapi.service.client.invoker.Configuration
import io.igolide.blueprints.openapi.service.client.service.AccountApi
import io.igolide.blueprints.openapi.utils.authenticate

interface ApiClients {
    val port: Int
    val host: String
        get() = System.getenv("APP_HOST") ?: "localhost"

    @Suppress("HttpUrlsUsage")
    private val client: ApiClient
        get() {
            return Configuration.getDefaultApiClient().also {
                it.basePath = "http://$host:$port"
            }
        }

    val accountsService: AccountApi
        get() = AccountApi(client).also { it.apiClient.authenticate() }
}

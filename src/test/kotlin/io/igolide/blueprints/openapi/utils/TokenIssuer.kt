package io.igolide.blueprints.openapi.utils

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import java.security.KeyPair
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.sql.Timestamp

object TokenIssuer {
    private lateinit var publicKey: RSAPublicKey
    private lateinit var privateKey: RSAPrivateKey
    private lateinit var algorithm: Algorithm

    fun init(keyPair: KeyPair) {
        publicKey = keyPair.public as RSAPublicKey
        privateKey = keyPair.private as RSAPrivateKey
        algorithm = Algorithm.RSA256(publicKey, privateKey)
    }

    fun issue(subject: String, roles: Array<String> = arrayOf("user")): String {
        val iat = Timestamp(System.currentTimeMillis())
        val eat = Timestamp(iat.time + 1000 * 60 * 60 * 24)
        return JWT.create()
            .withSubject(subject)
            .withExpiresAt(eat)
            .withIssuedAt(iat)
            .withArrayClaim("roles", roles)
            .sign(algorithm)
    }
}

package io.igolide.blueprints.openapi.utils

import io.igolide.blueprints.openapi.dataProviders.AuthTokenDataProvider
import io.igolide.blueprints.openapi.service.client.invoker.ApiClient
import io.igolide.blueprints.openapi.service.client.invoker.auth.HttpBearerAuth

fun ApiClient.authenticate() {
    val bearerAuth = this.getAuthentication("bearerAuth") as HttpBearerAuth
    bearerAuth.bearerToken = AuthTokenDataProvider.validLongLiveToken()
}

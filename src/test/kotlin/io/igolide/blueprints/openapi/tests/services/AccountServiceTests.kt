package io.igolide.blueprints.openapi.tests.services

import io.igolide.blueprints.openapi.core.SpringTests
import org.testng.Assert
import org.testng.annotations.Test

class AccountServiceTests : SpringTests() {
    @Test
    fun statusTest() {
        val accounts = accountsService.getAccounts(null, null, null)
        Assert.assertEquals(accounts.size, 20)
    }
}

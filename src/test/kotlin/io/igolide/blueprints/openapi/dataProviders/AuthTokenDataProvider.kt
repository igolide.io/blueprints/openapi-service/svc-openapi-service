package io.igolide.blueprints.openapi.dataProviders

import io.igolide.blueprints.openapi.utils.TokenIssuer

object AuthTokenDataProvider {
    private val issuer = TokenIssuer

    fun validLongLiveToken(): String {
        return issuer.issue("Voksiv", arrayOf("user"))
    }
}

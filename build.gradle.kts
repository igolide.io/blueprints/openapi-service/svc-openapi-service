import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.7.0"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    kotlin("jvm") version "1.6.21"
    kotlin("plugin.spring") version "1.6.21"
    id("org.jlleitschuh.gradle.ktlint") version "10.2.1"
    id("jacoco")
    id("org.ajoberstar.grgit") version "5.0.0"
    id("org.sonarqube") version "3.0"
}

group = "svc.openapi"
version = "0.0.1"
java.sourceCompatibility = JavaVersion.VERSION_17
val isCiRun = System.getenv("CI_JOB_TOKEN") != null
val currentBranch: String = if (isCiRun) System.getenv("CI_COMMIT_REF_NAME") else grgit.branch.current().fullName

configurations {
    all {
        exclude("org.springframework.boot", "spring-boot-starter-logging")
        exclude("org.slf4j", "slf4j-simple")
    }
}

repositories {
    mavenCentral()
    maven {
        url = uri("https://gitlab.com/api/v4/groups/igolide.io/-/packages/maven")
        name = "GitLab"
        credentials(HttpHeaderCredentials::class) {
            name = if (isCiRun) "Job-Token" else "Private-Token"
            value = if (isCiRun) System.getenv("CI_JOB_TOKEN") else System.getenv("GITLAB_PERSONAL_TOKEN")
        }
        authentication {
            create<HttpHeaderAuthentication>("header")
        }
    }
}

dependencies {
    implementation("io.igolide.blueprints:openapi.service.server:1.0.0")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.security:spring-security-core")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.security:spring-security-oauth2-resource-server")
    implementation("org.springframework.boot:spring-boot-starter-log4j2")
    implementation("com.auth0:java-jwt:3.19.2")

    testImplementation("io.igolide.blueprints:openapi.service.client:1.0.0")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.testng:testng:7.6.0")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "17"
    }
}

tasks.withType<Test> {
    useTestNG()
}

tasks.jacocoTestReport {
    reports {
        xml.required.set(true)
    }
}

tasks.jacocoTestCoverageVerification {
    violationRules {
        rule {
            limit {
                minimum = 0.7.toBigDecimal()
            }
        }
    }
}

tasks.getByName("test").finalizedBy("jacocoTestReport")
tasks.getByName("jacocoTestReport").finalizedBy("jacocoTestCoverageVerification")

sonarqube {
    properties {
        property("sonar.projectKey", "igolide.io_svc-openapi-service")
        property("sonar.organization", "igolideio")
        property("sonar.branch.name", currentBranch)
        property("sonar.coverage.jacoco.xmlReportPaths", "build/reports/jacoco/test/jacocoTestReport.xml")
    }
}

tasks.getByName("check").finalizedBy("sonarqube")

tasks.withType<Jar> {
    archiveFileName.set("svc.jar")
}
